<?php
declare (strict_types = 1);

/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Webman\Route;



include_path(app_path() . DIRECTORY_SEPARATOR . 'route');



//鹰：
//  注意：
//  和TP不同的是：1、路由是 / 开头，否则出错；转向地址是数组[类, 方法]；2、middleware是数组，不可以链式调用；

/*/ 设置$uri的任意方法请求的路由
Route::any($uri, $callback);
// 设置$uri的get请求的路由
Route::get($uri, $callback);
// 设置$uri的请求的路由
Route::post($uri, $callback);
// 设置$uri的put请求的路由
Route::put($uri, $callback);
// 设置$uri的patch请求的路由
Route::patch($uri, $callback);
// 设置$uri的delete请求的路由
Route::delete($uri, $callback);
// 设置$uri的head请求的路由
Route::head($uri, $callback);
// 同时设置多种请求类型的路由
Route::add(['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD', 'OPTIONS'], $uri, $callback);
// 分组路由
Route::group($path, $callback);
// 资源路由
Route::resource($path, $callback, [$options]);
// 回退路由，设置默认的路由兜底
Route::fallback($callback);

Route::fallback(function(){
    return redirect('/');
    //return json(['code' => 404, 'msg' => '404 not found']);
});


//闭包路由。由于闭包函数不属于任何控制器，所以$request->app $request->controller $request->action 全部为空字符串。
Route::any('/test', function ($request) {
    return response('test');
});

//类路由
Route::any('/testclass', [app\controller\Index::class, 'test']);



资源型路由
Route::resource('/test', app\controller\Index::class);
//指定资源路由
Route::resource('/test', app\controller\Index::class, ['index','create']);
//非定义性资源路由
// 如 notify 访问地址则为any型路由 /text/notify或/text/notify/{id} 都可 routeName为 test.notify
Route::resource('/test', app\controller\Index::class, ['index','create','notify']);

Verb	URI	Action	Route Name
GET	/test	index	test.index
GET	/test/create	create	test.create
POST	/test	store	test.store
GET	/test/{id}	show	test.show
GET	/test/{id}/edit	edit	test.edit
PUT	/test/{id}	update	test.update
DELETE	/test/{id}	destroy	test.destroy
PUT	/test/{id}/recovery	recovery	test.recovery

路由参数
鹰：使用{}匹配参数，[]表示可选。
  {var}的名称貌似可以和参数名不匹配

如果路由中存在参数，通过{key}来匹配，匹配结果将传递到对应的控制器方法参数中(从第二个参数开始依次传递)，例如：

// 匹配 /user/123 /user/abc
Route::any('/user/{id}', [app\controller\User::class, 'get']);

// 匹配 /user/123, 不匹配 /user/abc
Route::any('/user/{id:\d+}', function ($request, $id) {
    return response($id);
});

// 匹配 /user/foobar, 不匹配 /user/foo/bar
Route::any('/user/{name}', function ($request, $name) {
   return response($name);
});

// 匹配 /user /user/123 和 /user/abc
Route::any('/user[/{name}]', function ($request, $name = null) {
   return response($name ?? 'tom');
});


路由分组
group可嵌套使用
有时候路由包含了大量相同的前缀，这时候我们可以用路由分组来简化定义。例如：
Route::group('/blog', function () {
   Route::any('/create', function ($rquest) {return response('create');});
   Route::any('/edit', function ($rquest) {return response('edit');});
   Route::any('/view/{id}', function ($rquest, $id) {return response("view $id");});
});

路由中间件
可以给某个一个或某一组路由设置中间件。
Route::any('/admin', [app\admin\controller\Index::class, 'index'])->middleware([
    app\middleware\MiddlewareA::class,
    app\middleware\MiddlewareB::class,
]);

Route::group('/blog', function () {
   Route::any('/create', function () {return response('create');});
   Route::any('/edit', function () {return response('edit');});
   Route::any('/view/{id}', function ($r, $id) {response("view $id");});
})->middleware([
    app\middleware\MiddlewareA::class,
    app\middleware\MiddlewareB::class,
]);

注意:
->middleware() 路由中间件作用于 group 分组之后时候，当前路由必须在处于当前分组之下
鹰：也就是说，嵌套路由时，必须给里面的group中间件，而不是最外层。




示例：

Route::any('/test', function($request) {
    $route = $request->route;
    if ($route) {
        var_export($route->getPath());
        var_export($route->getMethods());
        var_export($route->getName());
        var_export($route->getMiddleware());
        var_export($route->getCallback());
        var_export($route->param()); // 此特性需要 webman-framework >= 1.3.16
    }

    return ret_value(0, "hi");
})
//->middleware(\app\middleware\AdminAuthTokenMiddleware::class)
//->middleware(\app\middleware\AdminAuthPermissionMiddleware::class)
//->middleware(\app\middleware\AdminLogMiddleware::class)
;

//{}表示必备变量，但变量名无限制（只是和控制器方法一一对应）
//[]表示可选，此时注意 对应的控制器方法参数，要有默认值
Route::any('/test/{name_no_limit}', [app\controller\test\Index::class, 'index'])->middleware([
    app\middleware\TestRouteMiddleware::class,
]);
Route::any('/test/test/{v1}/{v2}[/{v3}]', [app\controller\test\Index::class, 'testRoute'])->middleware([
    app\middleware\TestRouteMiddleware::class,
]);


*/



//鹰：废弃
//  也可以使用自动路由，但也得注意是否和多应用模式冲突（冲突时webman无法启动）
//  使用自动路由，貌似就不能手动路由了（比如分组、加入路由中间件等）
//include_once "route_automatic.php";
