<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

 //中间件执行顺序为 全局中间件->应用中间件->路由中间件。
return [
    // 全局中间件（默认调用）
    '' => [
        // ... 这里省略其它中间件
        //app\middleware\AccessControlMiddleware::class,
    ]
    //其他应用中间件
    //‘应用名' => [。。。, 。。。],
];
