<?php
declare (strict_types = 1);



// 自定义配置
return [

    'captcha' => [
        'prefix' => 'captcha_',
        'expire' => 180,
    ],


    //工程名
    'project_name'    =>  "Leamus",
    // 密码盐
    'password_salt'    => "Leamus",
    


	//JWT的加密密钥（对称加密）
	'JWT_Password' => "Leamus",
	'JWT_Audience' => ["Leamus"],
	'JWT_Issuser' => 'leamus.cn',
	'JWT_Subject' => 'Lms_ManagementPlatform_v1.0',







    'index_page' => '/',
    'login_page' => '/',

];
