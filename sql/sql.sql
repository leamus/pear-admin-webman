/*
 Navicat Premium Data Transfer

 Source Server         : host2.leamus.cn_SSH
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : chuliuxiang

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 04/09/2022 09:09:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lms_clx_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `lms_clx_admin_log`;
CREATE TABLE `lms_clx_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '管理员操作记录ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `admin_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '管理员姓名（off）',
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接',
  `page` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行为（鹰：off，页面名称）',
  `method` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '鹰：访问类型（原CRMEB为TP多应用的应用名）',
  `params` varchar(2083) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '鹰：参数',
  `ip` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录IP',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型（鹰：off，都是system）',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '操作时间',
  `agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器类型',
  `merchant_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `add_time`(`create_time`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员操作记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lms_clx_admin_log
-- ----------------------------
INSERT INTO `lms_clx_admin_log` VALUES (1, 1, NULL, '/adminapi/v1/setting/user', NULL, 'GET', '[]', '117.36.10.7', NULL, '2022-08-30 10:11:50', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', 0);
INSERT INTO `lms_clx_admin_log` VALUES (2, 1, NULL, '/adminapi/v1/setting/role', NULL, 'GET', '[]', '117.36.10.7', NULL, '2022-08-30 10:11:53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', 0);
INSERT INTO `lms_clx_admin_log` VALUES (3, 1, NULL, '/adminapi/v1/setting/permission', NULL, 'GET', '[]', '117.36.10.7', NULL, '2022-08-30 10:11:56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.70', 0);
INSERT INTO `lms_clx_admin_log` VALUES (4, 1, NULL, '/adminapi/v1/setting/user', NULL, 'GET', '[]', '111.19.92.37', NULL, '2022-08-30 15:59:21', 'Mozilla/5.0 (Linux; Android 8.0.0; MHA-AL00 Build/HUAWEIMHA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36', 0);
INSERT INTO `lms_clx_admin_log` VALUES (5, 1, NULL, '/adminapi/v1/setting/role', NULL, 'GET', '[]', '111.19.92.37', NULL, '2022-08-30 15:59:28', 'Mozilla/5.0 (Linux; Android 8.0.0; MHA-AL00 Build/HUAWEIMHA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36', 0);
INSERT INTO `lms_clx_admin_log` VALUES (6, 1, NULL, '/adminapi/v1/setting/permission', NULL, 'GET', '[]', '111.19.92.37', NULL, '2022-08-30 15:59:34', 'Mozilla/5.0 (Linux; Android 8.0.0; MHA-AL00 Build/HUAWEIMHA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36', 0);
INSERT INTO `lms_clx_admin_log` VALUES (7, 1, NULL, '/adminapi/v1/setting/user', NULL, 'GET', '[]', '113.137.161.22', NULL, '2022-08-30 17:44:02', 'Mozilla/5.0 (Linux; Android 8.0.0; MHA-AL00 Build/HUAWEIMHA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36', 0);
INSERT INTO `lms_clx_admin_log` VALUES (8, 1, NULL, '/adminapi/v1/setting/user', NULL, 'GET', '[]', '112.64.51.55', NULL, '2022-08-31 11:15:54', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36', 0);
INSERT INTO `lms_clx_admin_log` VALUES (9, 1, NULL, '/adminapi/v1/setting/role', NULL, 'GET', '[]', '112.64.51.55', NULL, '2022-08-31 11:15:57', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36', 0);
INSERT INTO `lms_clx_admin_log` VALUES (10, 1, NULL, '/adminapi/v1/setting/permission', NULL, 'GET', '[]', '112.64.51.55', NULL, '2022-08-31 11:15:59', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36', 0);

-- ----------------------------
-- Table structure for lms_clx_admin_permission
-- ----------------------------
DROP TABLE IF EXISTS `lms_clx_admin_permission`;
CREATE TABLE `lms_clx_admin_permission`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `pid` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id（鹰：一般是菜单的）',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '鹰：名称',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为菜单 1菜单 2功能',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '鹰：状态：0禁止；1启用；-1删除；其他根据业务',
  `sort` smallint(3) NOT NULL DEFAULT 1 COMMENT '排序',
  `module` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名',
  `controller` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '控制器',
  `action` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法名',
  `api_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '鹰：api接口地址',
  `methods` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '提交方式：POST GET PUT DELETE',
  `params` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `show` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为隐藏菜单（前台都使用）（隐藏菜单0=隐藏菜单,1=显示菜单）',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `menu_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '按钮路径',
  `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由名称 前端使用（鹰：貌似没用）',
  `unique_auth` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前台唯一标识',
  `access` tinyint(1) UNSIGNED NULL DEFAULT NULL COMMENT '子管理员是否可用',
  `header` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '顶部菜单标示',
  `is_header` tinyint(1) NULL DEFAULT NULL COMMENT '是否顶部菜单1是0否',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `delete_time` timestamp(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `access`(`access`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 620 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lms_clx_admin_permission
-- ----------------------------
INSERT INTO `lms_clx_admin_permission` VALUES (1, 0, '主页', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-home', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (2, 0, '设置', 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-set', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (3, 2, '管理权限', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-vercode', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (11, 1, '控制台', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-console', 'view/console/console1.html', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (31, 3, '角色管理', 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-group', 'view/system/role.html', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (32, 3, '管理员列表', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-friends', 'view/system/user.html', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (33, 3, '权限规则', 1, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'layui-icon-ok-circle', 'view/system/permission.html', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (101, 32, '新增管理员', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/User', 'POST', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:14:39', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (102, 32, '修改管理员', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/User/{id}', 'PUT', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:14:50', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (103, 32, '删除管理员', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/User/{id}', 'DELETE', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:14:57', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (104, 32, '获取管理员', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/User/{id}', 'GET', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:14:59', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (105, 32, '获取管理员列表', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/User', 'GET', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:04', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (201, 31, '新增角色', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Role', 'POST', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:06', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (202, 31, '修改角色', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Role/{id}', 'PUT', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:10', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (203, 31, '删除角色', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Role/{id}', 'DELETE', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:19', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (204, 31, '获取角色', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Role/{id}', 'GET', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:21', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (205, 31, '获取角色列表', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Role', 'GET', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:23', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (301, 33, '新增权限', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Permission', 'POST', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:25', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (302, 33, '修改权限', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Permission/{id}', 'PUT', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:35', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (303, 33, '删除权限', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Permission/{id}', 'DELETE', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:37', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (304, 33, '获取权限', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Permission/{id}', 'GET', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:38', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (305, 33, '获取权限列表', 2, 1, 10, NULL, NULL, NULL, '/adminapi/v1/setting/Permission', 'GET', NULL, 1, 'layui-icon-face-smile', '', NULL, NULL, NULL, NULL, NULL, '2022-08-30 10:15:32', NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (601, 0, '用户管理', 1, 1, 20, NULL, NULL, NULL, '', '', NULL, 1, 'layui-icon-username', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (602, 0, '商品管理', 1, 1, 40, NULL, NULL, NULL, '', '', NULL, 1, 'layui-icon-component', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (603, 0, '商户管理', 1, 1, 30, NULL, NULL, NULL, '', '', NULL, 1, 'layui-icon-404', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (604, 0, '订单管理', 1, 1, 50, NULL, NULL, NULL, '', '', NULL, 1, 'layui-icon-file', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `lms_clx_admin_permission` VALUES (605, 0, '配送管理', 1, 1, 60, NULL, NULL, NULL, '', '', NULL, 1, 'layui-icon-release', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for lms_clx_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `lms_clx_admin_role`;
CREATE TABLE `lms_clx_admin_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '身份管理id',
  `role_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份管理名称',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '身份管理权限(menus_id)',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `level` tinyint(3) UNSIGNED NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '鹰：状态：0禁止；1启用；-1删除；其他根据业务',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` timestamp(0) NULL DEFAULT NULL,
  `delete_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `role_name`(`role_name`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '身份管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lms_clx_admin_role
-- ----------------------------
INSERT INTO `lms_clx_admin_role` VALUES (1, '管理员账号', '1,11,2,3,32,101,102,103,104,105,31,201,202,203,204,205,33,301,302,303,304,305,601,603,602,604,605', '', 0, 1, '2022-08-10 14:50:42', '2022-08-10 14:50:43', NULL);
INSERT INTO `lms_clx_admin_role` VALUES (2, '临时', '1,2,3,11,31,32,33,101,102,103,104,105,201,202,203,204,205,301,302,303,304,305', NULL, 1, 1, '2022-08-10 12:29:52', '2022-08-10 12:29:52', NULL);
INSERT INTO `lms_clx_admin_role` VALUES (3, '销售经理', '1,2,3,11,31,32,33,101,102,103,104,105,201,202,203,204,205,301,302,303,304,305', NULL, 1, 1, '2022-07-22 19:36:05', NULL, NULL);

-- ----------------------------
-- Table structure for lms_clx_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `lms_clx_admin_user`;
CREATE TABLE `lms_clx_admin_user`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '后台管理员表ID',
  `account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '后台管理员账号',
  `pwd` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '后台管理员密码',
  `real_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '后台管理员姓名',
  `head_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `roles` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '后台管理员角色',
  `level` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '后台管理员级别（0为最高admin，不用验权）',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '鹰：状态：0禁止；1启用；-1删除；其他根据业务（后台管理员状态 1有效0无效）',
  `login_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `last_ip` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后台管理员最后一次登录ip',
  `last_time` timestamp(0) NULL DEFAULT NULL COMMENT '后台管理员最后一次登录时间',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '后台管理员添加时间',
  `update_time` timestamp(0) NULL DEFAULT NULL,
  `delete_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lms_clx_admin_user
-- ----------------------------
INSERT INTO `lms_clx_admin_user` VALUES (1, 'admin', '410cdc6dd8ae67dccee406ee505b56de', 'admin', '', '1', 0, 1, 1, '112.65.48.155', '1970-01-20 11:30:34', '2022-08-10 08:48:45', '2022-08-10 08:48:46', NULL);
INSERT INTO `lms_clx_admin_user` VALUES (2, 'test', '410cdc6dd8ae67dccee406ee505b56de', 'leamus', '', '2', 1, 1, 0, '', NULL, '2022-07-22 19:33:00', '2022-07-22 19:33:00', NULL);

SET FOREIGN_KEY_CHECKS = 1;
