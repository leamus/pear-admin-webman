<?php
declare (strict_types = 1);

namespace app\dao;



use app\model\BaseModel;

use think\exception\ValidateException;
use think\Model;

use think\helper\Str;



/**
 * Class BaseDao
 * @package app\dao
 */
abstract class BaseDao
{
    /**
     * 模型注入
     * @var object
     */
    protected $model;

    /**
     * 当前表名别名
     * @var string
     */
    protected $alias;

    /**
     * join表别名
     * @var string
     */
    protected $joinAlis;


    /**
     * 获取当前模型
     * @return string
     */
    //abstract protected function setModel(): string;

    /**
     * 设置join链表模型
     * @return string
     */
    protected function setJoinModel(): string
    {
    }

    /**
     * 读取数据条数
     * @param array $where
     * @return int
     */
    public function count(array $where = []): int
    {
        return $this->search($where)->count();
    }


    /** 鹰：加到这里了；和selectList的区别是使用search
     * 指定条件 $where 获取 $field（字符串） ，用 $page 和 $limit 分页，以多维数组（$key 为索引）形式返回
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param string $field
     * @return array
     */
    public function getList(array $where=[], int $page=null, int $limit=null, $field="*", $orderBy = null)//: array
    {
        return $this->
            search($where)->
            field($field)->
            when($page && $limit, function ($query) use ($page, $limit) {
                $query->page($page, $limit);
            })->
            when($orderBy, function ($query) use ($orderBy) {
                $query->order($orderBy[0], $orderBy[1]);
            })->
            //field("id, name, type, status, sort, api_path, show, icon, route")->
            select();
        //return $this->getModel()->where('delete_time', null)->select()->toArray();
    }
    
    /** 鹰：where的数组可以用 关联数组 和 二维数组（每个数组都必须是3个元素）；但不能混合使用。
     *   没有的条件字段会报错；
     * 获取某些条件数据
     * @param array $where
     * @param string $field
     * @param int $page
     * @param int $limit
     */
    public function selectList(array $where, $page = 0, $limit = 0, $field = '*', $orderBy = null)
    {
        return $this->getModel()->
            where($where)->
            field($field)->
            when($page && $limit, function ($query) use ($page, $limit) {
                $query->page($page, $limit);
            })->
            when($orderBy, function ($query) use ($orderBy) {
                $query->order($orderBy[0], $orderBy[1]);
            })->
            select();
    }


    /**
     * 获取某些条件总数
     * @param array $where
     */
    public function getCount(array $where)
    {
        return $this->getModel()->where($where)->count();
    }

    /**
     * 获取某些条件去重总数
     * @param array $where
     * @param $field
     * @param $search
     */
    public function getDistinctCount(array $where, $field, $search = true)
    {
        if ($search) {
            return $this->search($where)->field('COUNT(distinct(' . $field . ')) as count')->select()->toArray()[0]['count'] ?? 0;
        } else {
            return $this->getModel()->where($where)->field('COUNT(distinct(' . $field . ')) as count')->select()->toArray()[0]['count'] ?? 0;
        }
    }

    /**
     * 获取模型
     * @return BaseModel
     */
    protected function getModel()
    {
        return $this->model;
        //return app()->make($this->setModel());
    }

    /**
     * 获取主键
     * @return mixed
     */
    protected function getPk()
    {
        return $this->getModel()->getPk();
    }

    /**
     * 获取一条数据
     * 鹰：根据条件获取一条数据
     *   where可以是id，也可以是一个数组；with是其他数据库（连接）
     * @param int|array $where
     * @param array|string|null $field
     * @return array|Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get($where, $field = '*', ?array $with = [])
    {
        ///$field = explode(',', $field);
        ///$field ?? ['*']

        if (!is_array($where)) {
            $where = [$this->getPk() => $where];
        }

        return $this->getModel()::where($where)->when(count($with), function ($query) use ($with) {
            $query->with($with);
        //})->field($field)->find();
        })->field($field ?? ['*'])->find();
    }

    /**
     * 查询一条数据是否存在
     * @param $map
     * @param string $field
     * @return bool 是否存在
     */
    public function be($map, string $field = '')
    {
        if (!is_array($map) && empty($field)) $field = $this->getPk();
        $map = !is_array($map) ? [$field => $map] : $map;
        return 0 < $this->getModel()->where($map)->count();
    }


    /**
     * 获取单个字段值
     * @param array $where
     * @param string|null $field
     * @return mixed
     */
    public function value(array $where, ?string $field = '')
    {
        $pk = $this->getPk();
        return $this->getModel()::where($where)->value($field ?: $pk);
    }

    
    /** 鹰：加到这里了，和下面的区别是使用search
     * 指定条件 $where 获取 $field 以一维数组（$key 为索引）形式返回
     * @param array $where
     * @param string $field
     * @param string $key
     * @return array
     */
    public function column(array $where, string $field = "id", string $key = null)
    {
        return $this->search($where)->column($field, $key);
    }

    /**
     * 获取某个字段数组
     * @param array $where
     * @param string $field
     * @param string $key
     * @return array
     */
    public function getColumn(array $where, string $field, string $key = '')
    {
        return $this->getModel()::where($where)->column($field, $key);
    }


    /**
     * 删除
     * @param int|string|array $id
     * @return mixed
     */
    public function delete($id, ?string $key = null)
    {
        if (is_array($id)) {
            $where = $id;
        } else {
            $where = [is_null($key) ? $this->getPk() : $key => $id];
        }
        return $this->getModel()::where($where)->delete();
    }

    /**
     * 更新数据
     * @param int|string|array $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data, ?string $key = null)
    {
        if (is_array($id)) {
            $where = $id;
        } else {
            $where = [is_null($key) ? $this->getPk() : $key => $id];
        }
        return $this->getModel()::update($data, $where);
    }

    /**
     * 批量更新数据
     * @param array $ids
     * @param array $data
     * @param string|null $key
     * @return BaseModel
     */
    public function batchUpdate(array $ids, array $data, ?string $key = null)
    {
        return $this->getModel()::whereIn(is_null($key) ? $this->getPk() : $key, $ids)->update($data);
    }

    /**
     * 插入数据
     * @param array $data
     * @return mixed
     */
    public function save(array $data)
    {
        return $this->getModel()::create($data);
    }

    /**
     * 插入数据
     * @param array $data
     * @return mixed
     */
    public function saveAll(array $data)
    {
        return $this->getModel()::insertAll($data);
    }

    /**
     * 获取某个字段内的值
     * @param $value
     * @param string $filed
     * @param string $valueKey
     * @param array|string[] $where
     * @return mixed
     */
    public function getFieldValue($value, string $filed, ?string $valueKey = '', ?array $where = [])
    {
        return $this->getModel()->getFieldValue($value, $filed, $valueKey, $where);
    }

    /**
     * 获取搜索器和搜索条件key（getSearchData）
     * 鹰：分离 有和没有 searchXxxAttr 的搜索条件
     * @param array $withSearch
     * @return array[]
     * @throws \ReflectionException
     */
    private function separateSearchKey(array $withSearch)
    {
        $hasSearchAttr = [];
        $hasNoSearchAttr = [];
        $modelClass = new \ReflectionClass(get_class($this));
        foreach ($withSearch as $fieldName) {
            $method = 'search' . Str::studly($fieldName) . 'Attr';
            if ($modelClass->hasMethod($method)) {    //鹰：如果有这个 方法
                $hasSearchAttr[] = $fieldName;
            } else {
                $hasNoSearchAttr[] = $fieldName;
            }
        }
        return [$hasSearchAttr, $hasNoSearchAttr];
    }

    /**
     * 根据搜索器获取搜索内容
     * @param array $withSearch
     * @param array|null $data
     * @return Model
     */
    /*protected function withSearchSelect(array $withSearch, ?array $data = [])
    {
        //[$with] = $this->separateSearchKey($withSearch);
        //var_dump($with);
        //var_dump($withSearch);
        //var_dump($data);
        return $this->getModel()->withSearch($withSearch, $data);
    }
    */

    /**
     * 搜索
     * @param array $where
     * @return \BaseModel|mixed
     */
    //鹰：withSearch：
    //  1、有搜索器的调用搜索器，没有的按普通where来作为条件（和TP文档不太一样）；
    //  2、search的数组必须是关联数组（好像不能用三元的索引数组）；
    //  3、没有的条件字段会报错；
    protected function search(array $where = [])
    {
        if ($where) {
            //[$with] = $this->separateSearchKey($withSearch);
            return $this->getModel()->withSearch(array_keys($where), $where);
            ////return $this->withSearchSelect(array_keys($where), $where);
        } else {
            return $this->getModel();
        }
    }

    /**
     * 求和
     * @param array $where
     * @param string $field
     * @param bool $search
     * @return float
     */
    public function sum(array $where, string $field, bool $search = false)
    {
        if ($search) {
            return $this->search($where)->sum($field);
        } else {
            return $this->getModel()::where($where)->sum($field);
        }
    }

    /**
     * 高精度加法
     * @param int|string $key
     * @param string $incField
     * @param string $inc
     * @param string|null $keyField
     * @param int $acc
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function bcInc($key, string $incField, string $inc, string $keyField = null, int $acc = 2)
    {
        return $this->bc($key, $incField, $inc, $keyField, 1);
    }

    /**
     * 高精度 减法
     * @param int|string $uid id
     * @param string $decField 相减的字段
     * @param float|int $dec 减的值
     * @param string $keyField id的字段
     * @param bool $minus 是否可以为负数
     * @param int $acc 精度
     * @return bool
     */
    public function bcDec($key, string $decField, string $dec, string $keyField = null, int $acc = 2)
    {
        return $this->bc($key, $decField, $dec, $keyField, 2);
    }

    /**
     * 高精度计算并保存
     * @param $key
     * @param string $incField
     * @param string $inc
     * @param string|null $keyField
     * @param int $type
     * @param int $acc
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function bc($key, string $incField, string $inc, string $keyField = null, int $type = 1, int $acc = 2)
    {
        if ($keyField === null) {
            $result = $this->get($key);
        } else {
            $result = $this->get([$keyField => $key]);
        }
        if (!$result) return false;
        $new = 0;
        if ($type === 1) {
            $new = bcadd($result[$incField], $inc, $acc);
        } else if ($type === 2) {
            if ($result[$incField] < $inc) return false;
            $new = bcsub($result[$incField], $inc, $acc);
        }
        $result->{$incField} = $new;
        return false !== $result->save();
    }

    /**
     * 减库存加销量
     * @param array $where
     * @param int $num
     * @return mixed
     */
    public function decStockIncSales(array $where, int $num, string $stock = 'stock', string $sales = 'sales')
    {
        $isQuota = false;
        if (isset($where['type']) && $where['type']) {
            $isQuota = true;
            if (count($where) == 2) {
                unset($where['type']);
            }
        }
        $field = $isQuota ? 'stock,quota' : 'stock';

        $product = $this->getModel()->where($where)->field($field)->find();
        if ($product) {
            return $this->getModel()->where($where)->when($isQuota, function ($query) use ($num) {
                $query->dec('quota', $num);
            })->dec($stock, $num)->inc($sales, $num)->update();
        }
        return false;
    }

    /**
     * 加库存减销量
     * @param array $where
     * @param int $num
     * @return mixed
     */
    public function incStockDecSales(array $where, int $num, string $stock = 'stock', string $sales = 'sales')
    {
        $isQuota = false;
        if (isset($where['type']) && $where['type']) {
            $isQuota = true;
            if (count($where) == 2) {
                unset($where['type']);
            }
        }
        $salesOne = $this->getModel()->where($where)->value($sales);
        if ($salesOne) {
            $salesNum = $num;
            if ($num > $salesOne) {
                $salesNum = $salesOne;
            }
            return $this->getModel()->where($where)->when($isQuota, function ($query) use ($num) {
                $query->inc('quota', $num);
            })->inc($stock, $num)->dec($sales, $salesNum)->update();
        }
        return true;
    }
}
