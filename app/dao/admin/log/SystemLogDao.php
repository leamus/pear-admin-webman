<?php
declare (strict_types = 1);

namespace app\dao\admin\log;



use app\dao\BaseDao;
use app\model\admin\log\SystemLog;


use support\Container;



/**
 * 系统日志
 * Class SystemLogDao
 * @package app\dao\admin\log
 */
class SystemLogDao extends BaseDao
{
    /**
     * 构造方法
     * UserServices constructor.
     * @param SystemLogDao $dao
     */
    public function __construct()
    {
        $this->model = Container::get(SystemLog::class);
        //$this->model = new SystemLog;
    }

    
    /**
     * 删除过期日志
     * @throws \Exception
     */
    public function deleteLog()
    {
        $this->getModel()->where('add_time', '<', time() - 7776000)->delete();
    }

    /**
     * 获取系统日志列表
     * @param array $where
     * @param int $page
     * @param int $limit
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getLogList(array $where, int $page, int $limit)
    {
        return $this->search($where)->page($page, $limit)->order('add_time DESC')->select()->toArray();
    }

}
