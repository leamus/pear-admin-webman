<?php
declare (strict_types = 1);

namespace app\dao\admin\setting;



use app\dao\BaseDao;
use app\model\admin\setting\User;


use support\Container;



/**
 * 系统日志
 * Class UserDao
 * @package app\dao\admin
 */
class UserDao extends BaseDao
{
    /**
     * 构造方法
     * UserServices constructor.
     * @param UserDao $dao
     */
    public function __construct()
    {
        $this->model = Container::get(User::class);
        //$this->model = new User;
    }

}
