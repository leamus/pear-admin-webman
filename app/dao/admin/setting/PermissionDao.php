<?php
declare (strict_types = 1);

namespace app\dao\admin\setting;



use app\dao\BaseDao;
use app\model\admin\setting\Permission;


use support\Container;



/**
 * 系统日志
 * Class PermissionDao
 * @package app\dao\admin
 */
class PermissionDao extends BaseDao
{
    /**
     * 构造方法
     * UserServices constructor.
     * @param PermissionDao $dao
     */
    public function __construct()
    {
        $this->model = Container::get(Permission::class);
        //$this->model = new Permission;
    }

}
