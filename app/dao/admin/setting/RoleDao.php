<?php
declare (strict_types = 1);

namespace app\dao\admin\setting;



use app\dao\BaseDao;
use app\model\admin\setting\Role;


use support\Container;



/**
 * 系统日志
 * Class RoleDao
 * @package app\dao\admin
 */
class RoleDao extends BaseDao
{
    /**
     * 构造方法
     * UserServices constructor.
     * @param RoleDao $dao
     */
    public function __construct()
    {
        $this->model = Container::get(Role::class);
        //$this->model = new Role;
    }

}
