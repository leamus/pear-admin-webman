<?php
declare (strict_types = 1);

namespace app\middleware;



use app\services\admin\log\SystemLogServices;


use support\Container;


use Webman\MiddlewareInterface;
use Webman\Http\Request;
use Webman\Http\Response;



/**
 * 日志中間件
 * Class AdminLogMiddleware
 * @package app\adminapi\middleware
 */
class AdminLogMiddleware implements MiddlewareInterface
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function process(Request $request, callable $handler) : Response
    {
        $response = $handler($request);

        try {
            /** @var SystemLogServices $services */
            $services = Container::get(SystemLogServices::class);
            $services->recordAdminLog($request->AdminInfo['id'], null, null);
        } catch (\Throwable $e) {
            echo ($e);
        }
        return $response;
    }

}
