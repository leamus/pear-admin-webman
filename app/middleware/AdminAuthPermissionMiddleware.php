<?php
declare (strict_types = 1);

namespace app\middleware;



use Webman\MiddlewareInterface;
use Webman\Http\Request;
use Webman\Http\Response;



class AdminAuthPermissionMiddleware implements MiddlewareInterface
{
    /*
    use \app\common\traits\Base;

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function process(Request $request, callable $handler) : Response
    {
        //超级管理员不需要验证
        //if ($request->AdminInfo['id'] == 1) return $next($request);
        //if ($request->AdminInfo['level'] === 0) return $next($request);
        
        //没有这个路由，则直接返回（由miss路由处理）
        if($request->route->getPath() === null)
        {
            return $handler($request);
        }



        //当前匹配的路由 和 方法
        $rule = trim(strtolower($request->route->getPath()));
        $method = trim(strtolower($request->method()));

        //!!!跳过的 url
        if (in_array($rule, ['setting/admin/logout', 'menuslist']))
        {
            return $handler($request);
        }


        
        //!!!鹰：可以将其放入Redis
        $admin = \think\facade\Db::
        //connect('local')->
        name('admin_role')->
		field('rules, level')->
        whereIn('id', explode(',', $request->AdminInfo['roles']))->
        find()
        ;
        
        //超级管理员
        if ($admin["level"] === 0)
        {
            return $handler($request);
        }


        $ruleIds = explode(',', $admin["rules"]);
        //var_dump($ruleIds);
        
        //所有权限（包含api_url和method两个字段）
        $auth = \think\facade\Db::
        //connect('local')->
        name('admin_permission')->
		field('api_path, methods')->
        where('status', 1)->
        where('type', 2)->
        whereIn('id', $ruleIds)->
        where('delete_time', null)->
        select()->toArray()
		;
        //dump($auth);
        /*$auth = \think\facade\Db::connect('local')
        ->name('admin_permission')
		->field('api_path, methods')
        ->where('status', 1)
        ->where('type', 2)
        ->whereIn('id', $request->AdminInfo['rules'])
        ->where('delete_time', null)
        ->select()->toArray()
		;*/
        //$auth = $this->getRolesByAuth($request->AdminInfo['roles'], 2);



        //验证访问接口是否存在
        if (!in_array($rule,    //$rule是否在 $auth 的 所有 api_url 字段中
            array_map(function ($item) {    
                return trim(strtolower(str_replace(' ', '', $item)));   //去除空格、小写
            }, 
            array_column($auth, 'api_path'))))  //提取 $auth 的 api_url 字段
        {
            return ret_value(-1, "接口无权限", null, 400);
        }
        //验证访问接口是否有权限
        if (empty(
            array_filter($auth, function ($item) use ($rule, $method) { //过滤所有 $auth，不为空则有权
                //判断 api_url 和 method
                if (trim(strtolower($item['api_path'])) === $rule && $method === trim(strtolower($item['methods'])))
                    return true;
            })))
        {
            return ret_value(-2, "接口无权限", null, 401);
        }


        /*/验证权限（pear方法：用 控制器 和 方法名 验权，鹰：感觉不太好，还是用api的url来）
        //组装 root + 控制器 + 方法名
        $url = $request->root(). '/'.$request->controller(true).'/'.$request->action(true); 
        $href = array_column(Session::get('admin.menu'), 'href');
        if (!in_array($url, $href)) {
            return $request->isAjax()? $this->json('权限不足',999):$this->error('权限不足','');
        }
        */


        return $handler($request);
    }
}
