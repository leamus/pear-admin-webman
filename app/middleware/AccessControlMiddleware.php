<?php
declare (strict_types = 1);

namespace app\middleware;



use Webman\MiddlewareInterface;
use Webman\Http\Request;
use Webman\Http\Response;



class AccessControlMiddleware implements MiddlewareInterface
{
    public function process(Request $request, callable $handler) : Response
    {
        //echo 'AccessControlMiddleware In' . PHP_EOL;
        
        $response = $request->method() == 'OPTIONS' ? response('') : $handler($request);
        $response->withHeaders([
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Allow-Origin' => $request->header('Origin', '*'),
            'Access-Control-Allow-Methods' => '*',
            'Access-Control-Allow-Headers' => '*',
        ]);

        //echo 'AccessControlMiddleware Out' . PHP_EOL;

        return $response;
    }
}
