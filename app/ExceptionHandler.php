<?php
declare (strict_types = 1);

/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace app;



//use Webman\Exception\ExceptionHandler;
use Webman\Http\Request;
use Webman\Http\Response;


use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;

//use think\exception\Handle;
//use think\exception\HttpException;
//use think\exception\HttpResponseException;
use think\exception\ValidateException;


use Throwable;



/**
 * Class Handler
 * @package Support\Exception
 */
class ExceptionHandler extends \Webman\Exception\ExceptionHandler
{
    public $dontReport = [
        BusinessException::class,
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render(Request $request, Throwable $exception): Response
    {

        /*用法：
            throw new \think\exception\HttpException(66, '鹰歌');
            //或：abort(666, '鹰歌');

            //throw new \think\Exception('异常消息', 10006);
        */


        // 添加自定义异常处理机制

        // 参数验证错误
        // 鹰：可以统一在这里捕获（程序没有手动try-catch的情况下）
        if ($exception instanceof ValidateException) {
            return ret_value(-422, $exception->getMessage(), null, 422);
            //return json($exception->getError(), 422);
        }
        /*/ 请求异常
        if ($exception instanceof HttpException && $request->isAjax()) {
            return response($exception->getMessage(), $exception->getStatusCode());
        }
        */
        // RuntimeException
        if ($exception instanceof \RuntimeException) {
			return ret_value(-666, $exception->getMessage(), null, 500);
            //return response($exception->getMessage(), $exception->getStatusCode());
        }
        // Exception
        if ($exception instanceof \Exception) {

            $code = $exception->getCode();
            if ($request->expectsJson()) {
                $json = ['code' => $code ? $code : 500, 'msg' => $this->_debug ? $exception->getMessage() : 'Server internal error'];
                $this->_debug && $json['traces'] = (string)$exception;
                return new Response(500, ['Content-Type' => 'application/json'],
                    \json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
            }
            $error = $this->_debug ? \nl2br((string)$exception) : 'Server internal error';
            return new Response(500, [], $error);
            

            
			return ret_value(-999, $exception->getMessage(), null, 500);
            ///return response($exception->getMessage(), $exception->getStatusCode());
        }
        


        return parent::render($request, $exception);
    }

}