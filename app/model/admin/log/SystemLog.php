<?php
declare (strict_types = 1);

namespace app\model\admin\log;



use app\model\BaseModel;
use app\traits\ModelTrait;

use think\Model;



/**
 * 日志模型
 * Class SystemLog
 * @package app\model\system\log
 */
class SystemLog extends BaseModel
{
    use ModelTrait;

    //protected $connection = 'local';
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'admin_log';

    //鹰：貌似没用
    protected $insert = ['create_time'];

    
    protected $createTime = 'create_time';
    //protected $updateTime = 'update_time';

    /*/ 定义全局的查询范围
    protected $globalScope = ['status'];
    public function scopeStatus($query)
    {
        $query->where('status','>=',0);
    }*/
    


    /*protected function setCreateTimeAttr()
    {
        return time();
    }*/



    /**
     * 访问方式搜索器
     * @param Model $query
     * @param $value
     */
    public function searchPagesAttr($query, $value)
    {
        $query->whereLike('page', '%' . $value . '%');
    }

    /**
     * 访问路径搜索器
     * @param Model $query
     * @param $value
     */
    public function searchPathAttr($query, $value)
    {
        $query->whereLike('path', '%' . $value . '%');
    }

    /**
     * ip搜索器
     * @param Model $query
     * @param $value
     */
    public function searchIpAttr($query, $value)
    {
        $query->where('ip', 'LIKE', "%$value%");
    }

    /**
     * 管理员id搜索器
     * @param Model $query
     * @param $value
     */
    public function searchAdminIdAttr($query, $value)
    {
        $query->whereIn('admin_id', $value);
    }
}
