<?php
declare (strict_types = 1);

namespace app\model\admin\setting;



use app\model\BaseModel;
use app\traits\ModelTrait;

use think\Model;



/**
 * 管理员
 * Class User
 * @package app\model\admin
 */
class User extends BaseModel
{
    use ModelTrait;

    //protected $connection = 'local';

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'admin_user';

    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    //软删除，查询时会自动加上 xxx IS NULL
    //use \think\model\concern\SoftDelete;
    //protected $deleteTime = 'delete_time';
    //protected $defaultSoftDelete = 0;


    /*/ 定义全局的查询范围
    protected $globalScope = ['status'];
    public function scopeStatus($query)
    {
        $query->where('status','>=',0);
    }*/
    

    
    /**
     * 软删除
     * @param Model $query
     * @param $value
     */
    public function searchStatusAttr($query, $value, $data)
    {
        if ($value === null) {
            $query->where('status', '>=', 0);
        }
        else
            $query->where('status', $value);
    }
    
    /**
     * 管理员账号和姓名搜索器
     * @param Model $query
     * @param $value
     */
    public function searchNameLikeAttr($query, $value, $data)
    {
        if ($value) {
            $query->whereLike('account|real_name', '%' . $value . '%');
        }
    }
    
    /*/withSearch不能查询$value为null的，必须配置查询器
    public function searchDeleteTimeAttr($query, $value)
    {
        $query->where('delete_time', $value);
    }
    */
}
