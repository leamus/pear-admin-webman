<?php
declare (strict_types = 1);

namespace app\model;



use app\traits\ModelTrait;

use think\Model;



/**
 * Class BaseModel
 * @package app\mobel
 * @mixin ModelTrait
 */
class BaseModel extends Model
{

}
