<?php
declare (strict_types = 1);

namespace app;



//use think\App;
use think\exception\ValidateException;
use think\Validate;

use think\helper\Str;


use app\Request;



/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    //protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    //protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    //protected $middleware = [];


    
    /** 鹰：
     * 业务对象
     * @var
     */
    protected $services;



    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(/*App $app*/)
    {
        //$this->app     = $app;
        //$this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {}

    
    
    /**
     * 鹰：复制自 TP
     * 解析应用类的类名
     * @access public
     * @param string $layer 层名 controller model ...
     * @param string $name  类名
     * @return string
     */
    public static function parseClass(string $layer, string $name): string
    {
        $name  = str_replace(['/', '.'], '\\', $name);
        $array = explode('\\', $name);
        $class = Str::studly(array_pop($array));
        $path  = $array ? implode('\\', $array) . '\\' : '';

        return /*$this->namespace*/'app' . '\\' . $layer . '\\' . $path . $class;
    }

    /**
     * 验证数据
     * 鹰：OK !!!修改自 TP 的 BaseRequest
     * @access public
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    public static function validate(array $data, $validate, array $message = [], bool $batch = false, bool $exception = true)
    {
		/*$validate = \think\facade\Validate::rule('age', 'require')->
        rule([
            'name_like' => 'require',
        ]);
		if (!$validate->
            batch(true)->   //批量
            check($where)
            )
        {
			var_dump($validate->getError());
		}*/


        if (is_array($validate)) {  //普通直接验证
            $v = new Validate();
            $v->rule($validate);
        }
        else    //载入验证器类
        {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : static::parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch/* || $this->batchValidate*/) {
            $v->batch(true);
        }

        return $v->failException($exception)->check($data);
    }

}
