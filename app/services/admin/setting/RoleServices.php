<?php
declare (strict_types = 1);

namespace app\services\admin\setting;



use app\dao\admin\setting\RoleDao;

use app\services\BaseServices;
use app\traits\ServicesTrait;


use support\Container;



/**
 * 系统用户管理
 * Class RoleServices
 * @package app\services\admin
 * @method deleteLog() 定期删除日志
 */
class RoleServices extends BaseServices
{
    use ServicesTrait;
    

    /**
     * 构造方法
     * RoleServices constructor.
     */
    public function __construct()
    {
        $this->dao = Container::get(RoleDao::class);
        //$this->dao = new RoleDao;
    }


    //返回根据参数，返回List和count
    public function getList($where, $page, $limit, $field="*", $orderBy = null)
    {
        //return $this->getModel()->where('delete_time', null)->select()->toArray();
        $list = $this->dao->getList($where, $page, $limit, $field, $orderBy)->toArray();
        $count = $this->dao->count($where);

        
        //替换name

        //获得所有name，id为索引
        /** @var PermissionServices $service */
        $service = Container::get(\app\services\admin\setting\PermissionServices::class);
        $rules = $service->column([], "name", "id");
        //$rules = array_keys_value($rules, '1,2,3', true);
        
        //循环查找 rolesID并替换为 roles名称
        foreach ($list as &$item) {
            $item['rulesName'] = implode(',', array_keys_value($rules, $item['rules'], false, '【未知】'));
            //var_dump($item['rules']);
        }



        return compact('list', 'count');
        //$count = $this->dao->count($where);
        //return $this->dao->getList();
    }
}