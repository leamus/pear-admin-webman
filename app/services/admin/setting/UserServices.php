<?php
declare (strict_types = 1);

namespace app\services\admin\setting;



use app\dao\admin\setting\UserDao;

use app\services\BaseServices;
use app\traits\ServicesTrait;


use support\Container;



/**
 * 系统用户管理
 * Class UserServices
 * @package app\services\admin
 * @method deleteLog() 定期删除日志
 */
class UserServices extends BaseServices
{
    use ServicesTrait;
    

    /**
     * 构造方法
     * UserServices constructor.
     */
    public function __construct()
    {
        $this->dao = Container::get(UserDao::class);
        //$this->dao = new UserDao;
    }


    //返回根据参数，返回List和count
    public function getList($where, $page, $limit, $field="*", $orderBy = null)
    {
        //return $this->getModel()->where('delete_time', null)->select()->toArray();
        $list = $this->dao->getList($where, $page, $limit, $field, $orderBy)->toArray();
        $count = $this->dao->count($where);

        
        //替换role_name

        //获得所有role_name，id为索引
        /** @var RoleServices $service */
        $service = Container::get(\app\services\admin\setting\RoleServices::class);
        $roles = $service->column([], "role_name", "id");
        //$roles = array_keys_value($roles, '1,2,3', true);
        
        //循环替换
        foreach ($list as &$item) {
            $item['roles'] = implode(',', array_keys_value($roles, $item['roles'], true));
            //var_dump($item['roles']);
        }



        return compact('list', 'count');
        //$count = $this->dao->count($where);
        //return $this->dao->getList();
    }
}