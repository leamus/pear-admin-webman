<?php
declare (strict_types = 1);

namespace app\services\admin\setting;



use app\dao\admin\setting\PermissionDao;

use app\services\BaseServices;
use app\traits\ServicesTrait;


use support\Container;



/**
 * 系统用户管理
 * Class PermissionServices
 * @package app\services\admin
 * @method deleteLog() 定期删除日志
 */
class PermissionServices extends BaseServices
{
    use ServicesTrait;
    

    /**
     * 构造方法
     * PermissionServices constructor.
     */
    public function __construct()
    {
        $this->dao = Container::get(PermissionDao::class);
        //$this->dao = new PermissionDao;
    }

    
    //返回根据参数，返回List和count
    // public function getList(array $where=[], int $page=null, int $limit=null, $field="*")
    // {
    //     //return $this->getModel()->where('delete_time', null)->select()->toArray();
    //     return $this->dao->getList($where, null, null, $field);
    //     //$count = $this->dao->count($where);
    //     //return $this->dao->getList();
    // }



    
    /**
     * 修改管理员
     * @param int $id
     * @param array $data
     * @return bool
     */
    /*public function save1(int $id, array $data)
    {
        if (!$info = $this->dao->get($id)) {
            return -1;
        }
        // if ($adminInfo->is_del) {
        //     throw new AdminException('管理员已经删除');
        // }
        $info->pid = $data['pid'] ?? $info->pid;
        $info->name = $data['name'] ?? $info->name;
        $info->icon = $data['icon'] ?? $info->icon;
        $info->type = $data['type'] ?? $info->type;
        $info->status = $data['status'] ?? $info->status;
        $info->show = $data['show'] ?? $info->show;
        $info->api_path = $data['api_path'] ?? $info->api_path;
        $info->methods = $data['methods'] ?? $info->methods;
        $info->menu_path = $data['menu_path'] ?? $info->menu_path;
        $info->sort = $data['sort'] ?? $info->sort;
        $info->unique_auth = $data['unique_auth'] ?? $info->unique_auth;
        if ($info->save()) {
            //\think\facade\Cache::clear();
            return 0;
        } else {
            return -2;
        }
    }
    */
}