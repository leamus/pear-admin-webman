<?php
declare (strict_types = 1);

use Webman\Route;



Route::any('/adminapi/test', function() {
    return ret_value(0, "hi");
})
->middleware([\app\middleware\AdminAuthTokenMiddleware::class, \app\middleware\AdminAuthPermissionMiddleware::class, \app\middleware\AdminLogMiddleware::class])
;



/**
 * 无需授权的接口
 */
Route::group(function() {
    Route::any('/adminapi/v1/Login/getCaptcha', [app\controller\adminapi\v1\LoginController::class, 'getCaptcha']);
})
->middleware([\app\middleware\AccessControlMiddleware::class])
;



Route::group(function() {
    Route::any('/adminapi/v1/Login/login', [app\controller\adminapi\v1\LoginController::class, 'login']);
})
->middleware([\app\middleware\AccessControlMiddleware::class, \app\middleware\AdminLogMiddleware::class])
;



/**
 * 后台接口
 */
Route::group('/adminapi', function() {
    //Route::any('/login',[app\controller\adminapi\v1\setting\UserController::class, 'login']);
    
    //Route::any('/adminapi/v1/setting/User/getList', [app\controller\adminapi\v1\setting\UserController::class, 'getList']);
    Route::resource('/v1/setting/User', app\controller\adminapi\v1\setting\UserController::class, ['index', 'show', 'store', 'update', 'destroy']);
    //Route::any('/adminapi/v1/setting/Role/getList', [app\controller\adminapi\v1\setting\RoleController::class, 'getList']);
    Route::resource('/v1/setting/Role', app\controller\adminapi\v1\setting\RoleController::class, ['index', 'show', 'store', 'update', 'destroy']);
    //Route::any('/adminapi/v1/setting/Permission/index', [app\controller\adminapi\v1\setting\PermissionController::class, 'index']);
    //Route::any('/adminapi/v1/setting/Permission/save', [app\controller\adminapi\v1\setting\PermissionController::class, 'save']);
    Route::resource('/v1/setting/Permission', app\controller\adminapi\v1\setting\PermissionController::class, ['index', 'show', 'store', 'update', 'destroy']);
    //Route::delete('/adminapi/v1/setting/Permission', [app\controller\adminapi\v1\setting\PermissionController::class, 'delete']);

    Route::fallback(function() {
        return ret_value(-1, "404", null, 404);
    });
})
//多个中间件使用数组方式
->middleware([\app\middleware\AccessControlMiddleware::class, \app\middleware\AdminAuthTokenMiddleware::class, \app\middleware\AdminAuthPermissionMiddleware::class, \app\middleware\AdminLogMiddleware::class])
;
