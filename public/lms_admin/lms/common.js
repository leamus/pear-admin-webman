

var lms_config = {
	//api的基址
    api_url: "http://127.0.0.1:8086",
	//本后台前端的根目录
    base_path: "/lms_admin/",
}



function test() {

	let $ = layui.jquery;

	$.ajax({
		url: "http://ensor.com.cn", // 请求路径
		type: "GET", //请求方式
		data: "username=jack&age=23", //请求参数
		//data:{"username":"jack","age":23},

		success: function (data) {
			console.log(data);
		}, //响应成功后的回调函数
		error: function () {
			alert("出错啦...")
		}, //表示如果请求响应出现错误，会执行的回调函数

		dataType: "text" //设置接受到的响应数据的格式
	});



	let str = "深林孤鷹";
	//加密
	var res = base64.encode(str);
	console.log(res);

	//解密
	var res = base64.decode(res);
	console.log(res);

	console.log(Base64.decode('eyJleHAiOjE2NTYzODM5NDcsImlhdCI6MTY1NjI5NzU0NywiaXNzIjoibGVhbXVzLmNuIiwianRpIjoibHFnZXU0MDJybGxyb3JqMGFvb2xxMGdxcGM2MmI5MTg0YmNiZjkzMS44ODA0NjI4OSIsIm5iZiI6MTY1NjI5NzU0Nywic3ViIjoiTG1zX01hbmFnZW1lbnRQbGF0Zm9ybV92MS4wIiwicmVmcmVzaF90b2tlbl9leHBpcmUiOjE2NTY1NTY3NDcsImRhdGEiOnsibG9naW5uYW1lIjoicGxlYWZsZXMiLCJpZCI6MSwibmlja25hbWUiOiJcdTZkZjFcdTY3OTdcdTViNjRcdTllNzAiLCJzZXgiOjF9fQ'));




	layui.data('test', {
		key: 'nickname',
		value: '贤心'
	});
	layui.sessionData('test', {
		key: 'nickname',
		value: '贤心2'
	});

	console.debug(layui.data('test'));
	console.log(111);
	console.log(layui.data('test'));
	console.log(layui.sessionData('test'));
}




function isString(str) {
	if (typeof (str) === 'string')
		return true;
	return false;
}



//网页，从URL中获取param的方式
function getQueryString(name) { 
	let reg = `(^|&)${name}=([^&]*)(&|$)`;
    //let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i'); 
	let r = window.location.search.substr(1).match(reg); 
	if (r != null)
		return unescape(r[2]);
	return null; 
}


/*
** 产生任意长度随机字母数字组合
** min-任意长度最小位[固定位数] max-任意长度最大位
** xuanfeng 2014-08-28    深林孤鹰 2022-08-15
*/
function randomString(min, max=undefined, arr=undefined){
    let str = "";
    let range;
    //if(charSet === undefined)
    //    charSet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    arr = arr || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    // 随机位数
    if(max !== undefined){
        range = Math.round(Math.random() * (max - min)) + min;
    }
    else
        range = min;
    for(let i = 0; i < range; ++i){
        pos = Math.round(Math.random() * (arr.length - 1));
        str += arr[pos];
    }
    return str;
}



/**
 * 鹰：OK
 */
//保存、获取 用户信息
const setUserInfo = (userinfo) => {
	//localStorage 持久化存储：layui.data(table, settings)，数据会永久存在，除非物理删除。
	//sessionStorage 会话性存储：layui.sessionData(table, settings)，页面关闭后即失效。注：layui 2.2.5 新增
	if(userinfo === undefined || userinfo === null)
		layui.data('lms', {
			key: 'userInfo',
			remove: true
		});
	else
		layui.data('lms', {
			key: 'userInfo',
			value: userinfo
		});
}
const getUserInfo = () => {
	return layui.data('lms').userInfo;
}

/**
 * 检查登录，没有登录返回false，否则返回 用户信息对象
 * 鹰：OK
 * options.nav：为true则没有登录时跳转至登录界面。
 * @return {Boolean}
 */
const checkLogin = (options = {}) => {
	const userInfo = getUserInfo();
	if (typeof (userInfo) === 'object' && userInfo !== null) {
		return userInfo;
	}

	//是否需要自动跳转到login页面
	if (options.nav === true) {
		options.nav = lms_config.base_path + '/login.html';
	}
	if (isString(options.nav)) {
		//console.log("yes")
		///location.href = options.nav;
		top.location = options.nav;
		///parent.location = options.nav;
	}
	//else
	//	console.log("NO")
	return false;
}


/*
鹰：发送数据
如果没有登录，则返回false，并切换至登录页面
如果token过期，则自动刷新token并再次发送原请求
如果refresh_token也过期，则切换至登录页面
*/
//import base64 from './jquery.base64'
//登录后的数据请求
//自动加入header（token），且进行登录判断，如果登录异常则跳入登录界面
const $request = (params) => {

	let $ = layui.jquery;

	//console.log("check:", checkLogin());
	let userinfo = checkLogin({
		nav: true
	});
	if (userinfo === false) {
		//console.log("check:", checkLogin());
		return false;
	}

	return new Promise((resolve, reject) => {

		$.ajax({
			//uni.request({
				
			//url: BASE_URL + params.url,//请求路径
			//type: method,	//H5的请求方式
			///method: params.method || 'GET',	//uniapp的
			headers: {
				Authorization: userinfo["token"],
				Author: 'Leamus',
				//'Access-Control-Allow-Origin':'*',
			},
			//contentType: 'application/json',	//内容编码类型，默认为"application/x-www-form-urlencoded"
			//data: params.data || {},	//请求参数
        	//data: JSON.stringify(data.field),	//如果 contentType为Json，则必须序列化，反之不需要（自动转换为标准POST格式）

			//dataType: 'json',	//预期服务器返回的数据类型；设置接受到的响应数据的格式
			
			////Accept: "application/json; charset=utf-8",
			////traditional: true, //是否使用传统的方式浅层序列化,若有数组参数或对象参数需要设置true!!!!!!

			...params,

			success(ret) {
				//console.log("$request success!", ret);
				//if(typeof(success1) === 'function')
				//	success1(ret);

				//返回成功
				if (ret && ret.code === 0) {
					resolve(ret);
					return;
				}

				reject(ret);
				//that.loginSuccessCallBack(ret.data);
			},
			error(ret) {
				//console.log("$request fail!", ret);
				//if(typeof(fail1) === 'function')
				//	fail1(ret);

				//返回Token过期
				if (ret && ret.status === 401) {
					if (ret.responseJSON.code < 0) { //需要重新登录
						setUserInfo(null);
						location.href = lms_config.base_path + '/login.html';
						/*uni.navigateTo({
							url: '../login.html',
							success() {
								//console.log("success!!!");
							},
							fail(err) {
								//console.log("fail!!!", err);
							}
						});*/
						//reject(ret);
						//return;
					}
					//ret.data.code === 0 重新获取token：refreshToken
					else { //刷新Token

						//setUserInfo(null);

						/*uni.navigateTo({	//跳转到刷新Token页面
							url: '/pages/auth/refreshToken',
							success() {
								//console.log("success!!!");
							},
							fail(err) {
								//console.log("fail!!!", err);
							}
						});*/

						//console.log(ret.data, typeof(ret.data));
						//解析JWT（BASE64反解Payload）
						//let user = JSON.parse(Base64.decode(ret.token.split(".")[1])).data;
						//console.log(JSON.parse(u))

						setUserInfo({
							isLogin: true,
							"token": ret.data.token,
							info: ret.data.info,
							rules: ret.data.rules,
						});

						//console.log(getUserInfo());

						//将原来请求重新发送
						$request(params).then(resolve(ret)).catch(reject(ret));
						return;
					}
				}

				reject(ret);
				//globalData2("$util").msg(ret.msg);
			},
		});

		//console.log("working...")
	})
}


//制作菜单
//rules为服务器返回的所有权限数组（平行）
//type为1则只返回 菜单类型 的菜单，否则也返回 菜单和API 的菜单
//setKeysById：给特定id新增一个key并赋值value。如果为null则忽略
//  格式：[ids, key, value]	//ids是列表，Permission的id只要在这个列表内就赋值
const fMakeMenu = function(rules, type=1, setKeysById=null) {
	let retMenu = [];	//返回值（也是顶层menu）
	let tmpMenu = {};	//按id为key的menu
	for(let r of rules) {

		if(type === 1 && r.type !== 1) {	//api
			continue;
		}
		if(r.show !== 1)	//是否显示
			continue;


		//制作菜单：
		if(tmpMenu[r.id] === undefined)		//如果还不存在，则创建
			tmpMenu[r.id] = {type: 1};
		tmpMenu[r.id].id = r.id;
		tmpMenu[r.id].title = r.name;
		tmpMenu[r.id].icon = 'layui-icon ' + r.icon;
		tmpMenu[r.id].openType = "_iframe";
		tmpMenu[r.id].href = r.menu_path;


		//加入额外的keys
		if(setKeysById) {
			//如果这个id在给定的Id列表内
			if(setKeysById[0].indexOf(r.id.toString()) >= 0) {
				tmpMenu[r.id][setKeysById[1]] = setKeysById[2];	//给特定key赋值
			}
		}


		if(r.pid === 0) {	//顶层
			retMenu.push(tmpMenu[r.id]);
			continue;
		}
		if(tmpMenu[r.pid] === undefined) {	//子菜单
			tmpMenu[r.pid] = {children: []};	//创建父菜单
		}
		else if(tmpMenu[r.pid].children === undefined)
			tmpMenu[r.pid].children = [];
		tmpMenu[r.pid].type = 0;	//菜单
		tmpMenu[r.pid].children.push(tmpMenu[r.id]);	//加入父菜单 children
	}

	return retMenu;
}



const fGetLastNodes = function(rules) {

	let parentNodes = [];
	let lastNodes = [];
	let allNodes = [];

	
	for(let r of rules) {
		allNodes.push(r.id);
		if(parentNodes.indexOf(r.pid) < 0)
			parentNodes.push(r.pid);
	}

	console.log(123, parentNodes, allNodes)
	
	lastNodes = allNodes.filter(item=>{
		//差集
		return !parentNodes.includes(item);
		//交集
		//return parentNodes.includes(item)
	});

	return lastNodes;
}
